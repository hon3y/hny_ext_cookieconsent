<?php
namespace HNY\HnyExtCookieconsent\Controller;

/***
 *
 * This file is part of the "hny_ext_cookieconsent" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018
 *
 ***/

/**
 * RenderController
 */
class RenderController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * renderRepository
     *
     * @var \HNY\HnyExtCookieconsent\Domain\Repository\RenderRepository
     * @inject
     */
    protected $renderRepository = null;

    /**
     * action render
     *
     * @return void
     */
    public function renderAction()
    {

    }
}
