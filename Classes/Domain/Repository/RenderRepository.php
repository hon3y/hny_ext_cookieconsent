<?php
namespace HNY\HnyExtCookieconsent\Domain\Repository;

/***
 *
 * This file is part of the "hny_ext_cookieconsent" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018
 *
 ***/

/**
 * The repository for Renders
 */
class RenderRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{
    }
