<?php
namespace HNY\HnyExtCookieconsent\Domain\Model;

/***
 *
 * This file is part of the "hny_ext_cookieconsent" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018
 *
 ***/

/**
 * Render
 */
class Render extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    }
