<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'HNY.HnyExtCookieconsent',
            'Hnyextcookieconsentrender',
            'hny_ext_cookieconsent :: Render'
        );

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('hny_ext_cookieconsent', 'Configuration/TypoScript', 'hny_ext_cookieconsent');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_hnyextcookieconsent_domain_model_render', 'EXT:hny_ext_cookieconsent/Resources/Private/Language/locallang_csh_tx_hnyextcookieconsent_domain_model_render.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_hnyextcookieconsent_domain_model_render');

    }
);
