<?php
namespace HNY\HnyExtCookieconsent\Tests\Unit\Domain\Model;

/**
 * Test case.
 */
class RenderTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \HNY\HnyExtCookieconsent\Domain\Model\Render
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \HNY\HnyExtCookieconsent\Domain\Model\Render();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function dummyTestToNotLeaveThisFileEmpty()
    {
        self::markTestIncomplete();
    }
}
