<?php
namespace HNY\HnyExtCookieconsent\Tests\Unit\Controller;

/**
 * Test case.
 */
class RenderControllerTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \HNY\HnyExtCookieconsent\Controller\RenderController
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = $this->getMockBuilder(\HNY\HnyExtCookieconsent\Controller\RenderController::class)
            ->setMethods(['redirect', 'forward', 'addFlashMessage'])
            ->disableOriginalConstructor()
            ->getMock();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

}
