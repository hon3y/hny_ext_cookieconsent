
plugin.tx_hnyextcookieconsent_hnyextcookieconsentrender {
    view {
        # cat=plugin.tx_hnyextcookieconsent_hnyextcookieconsentrender/file; type=string; label=Path to template root (FE)
        templateRootPath = EXT:hny_ext_cookieconsent/Resources/Private/Templates/
        # cat=plugin.tx_hnyextcookieconsent_hnyextcookieconsentrender/file; type=string; label=Path to template partials (FE)
        partialRootPath = EXT:hny_ext_cookieconsent/Resources/Private/Partials/
        # cat=plugin.tx_hnyextcookieconsent_hnyextcookieconsentrender/file; type=string; label=Path to template layouts (FE)
        layoutRootPath = EXT:hny_ext_cookieconsent/Resources/Private/Layouts/
    }
    persistence {
        # cat=plugin.tx_hnyextcookieconsent_hnyextcookieconsentrender//a; type=string; label=Default storage PID
        storagePid =
    }
}
