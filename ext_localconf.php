<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'HNY.HnyExtCookieconsent',
            'Hnyextcookieconsentrender',
            [
                'Render' => 'render'
            ],
            // non-cacheable actions
            [
                'Render' => 'render'
            ]
        );

    // wizards
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
        'mod {
            wizards.newContentElement.wizardItems.plugins {
                elements {
                    hnyextcookieconsentrender {
                        icon = ' . \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('hny_ext_cookieconsent') . 'Resources/Public/Icons/user_plugin_hnyextcookieconsentrender.svg
                        title = LLL:EXT:hny_ext_cookieconsent/Resources/Private/Language/locallang_db.xlf:tx_hny_ext_cookieconsent_domain_model_hnyextcookieconsentrender
                        description = LLL:EXT:hny_ext_cookieconsent/Resources/Private/Language/locallang_db.xlf:tx_hny_ext_cookieconsent_domain_model_hnyextcookieconsentrender.description
                        tt_content_defValues {
                            CType = list
                            list_type = hnyextcookieconsent_hnyextcookieconsentrender
                        }
                    }
                }
                show = *
            }
       }'
    );
    }
);
